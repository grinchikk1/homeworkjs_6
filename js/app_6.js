"use strict";

function createNewUser() {
  const _firstName = prompt("Enter your firstname");
  const _lastName = prompt("Enter your lastname");
  const _birthday = prompt("Enter your birthday in format dd.mm.yyyy");
  const newUser = {
    _firstName,
    _lastName,
    _birthday,
    set firstName(value) {
      this._firstName = value;
    },
    get firstName() {
      return this._firstName;
    },
    set lastName(value) {
      this._lastName = value;
    },
    get lastName() {
      return this._lastName;
    },
    setFirstName(value) {
      this.firstName = value;
    },
    setLastName(value) {
      this.lastName = value;
    },
    getAge: function () {
      let day = _birthday.split(".");
      day = new Date(day[2], day[1] - 1, day[0]);
      let a = new Date();
      let dayNow = a.getTime();
      let b = new Date(day);
      let dayBirth = b.getTime();
      let age = dayNow - dayBirth;
      let fullAge = Math.floor(age / (1000 * 60 * 60 * 24 * 365.25));
      return fullAge;
    },
    getPassword: function () {
      return (
        this.firstName[0].toUpperCase() +
        this.lastName.toLowerCase() +
        this._birthday.slice(6)
      );
    },
    getlogin: function () {
      return (this.firstName[0] + this.lastName).toLowerCase();
    },
  };
  return newUser;
}

console.log(createNewUser().getAge());
console.log(createNewUser().getPassword());
